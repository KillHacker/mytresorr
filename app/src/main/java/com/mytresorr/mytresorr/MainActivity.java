package com.mytresorr.mytresorr;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, IPickResult {

    //String urlString = "http://10.0.2.2/api.php"; // URL to call
    String urlString = "http://www.mytresorr.com/api.php"; // URL to call
    String actionBarUrl = "http://www.mytresorr.com/"; // URL to call

    ImageButton ibSelectImage;
    EditText etName, etEmail, etPhone;
    Button bSubmit;

    Bitmap imageBitmap = null;

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ibSelectImage = findViewById(R.id.ibSelectImage);
        ibSelectImage.setOnClickListener(this);

        bSubmit = findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(this);

        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etMail);
        etPhone = findViewById(R.id.etPhone);

        Drawable newDrawable = bSubmit.getBackground();  // obtain Bg drawable from the button as a new drawable
        DrawableCompat.setTint(newDrawable, getResources().getColor(R.color.colorButton));  //set it's tint
        bSubmit.setBackground(newDrawable);  //apply back to button

        if (imageBitmap != null)
            ibSelectImage.setImageBitmap(imageBitmap);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibSelectImage:
                PickImageDialog.build(new PickSetup().setButtonOrientation(LinearLayoutCompat.HORIZONTAL)).setOnPickResult(this).show(this);

                break;
            case R.id.bSubmit:

                if (imageBitmap == null)
                    showAlertDialog("Please select your image!");
                else if (etName.getText().toString().isEmpty())
                    showAlertDialog("Please enter your name!");
                else if (etEmail.getText().toString().isEmpty())
                    showAlertDialog("Please enter your email!");
                else if (etPhone.getText().toString().isEmpty())
                    showAlertDialog("Please enter your phone number!");
                else {

                    HashMap<String, String> params = new HashMap<>();
                    params.put("action", "sendModelRequest");
                    params.put("image", encodeTobase64(imageBitmap));
                    params.put("name", etName.getText().toString());
                    params.put("email", etEmail.getText().toString());
                    params.put("phone", etPhone.getText().toString());

                    try {
                        new CallAPI().execute(hashMapToUrl(params));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                break;
        }
    }

    @Override
    protected void onPause() {
        SharedPreferences.Editor editor = getSharedPreferences("prefs", MODE_PRIVATE).edit();
        editor.putString("name", etName.getText().toString());
        editor.putString("email", etEmail.getText().toString());
        editor.putString("phone", etPhone.getText().toString());
        editor.apply();

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = getSharedPreferences("prefs", MODE_PRIVATE);
        etName.setText(sharedPreferences.getString("name", ""));
        etEmail.setText(sharedPreferences.getString("email", ""));
        etPhone.setText(sharedPreferences.getString("phone", ""));
    }

    private String hashMapToUrl(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {

            ibSelectImage.setImageBitmap(r.getBitmap());

            imageBitmap = r.getBitmap();
        } else {
            r.getError().printStackTrace();
            Toast.makeText(this, "Error opening image", Toast.LENGTH_SHORT).show();
        }
    }

    public void showAlertDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setPositiveButton("Ok", null);

        alertDialogBuilder.show();
    }

    public class CallAPI extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Uploading...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected String doInBackground(String... params) {

            String data = params[0]; //data to post
            String output = "";

            OutputStream out = null;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.show();
                }
            });

            try {

                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                out = new BufferedOutputStream(urlConnection.getOutputStream());

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                writer.write(data);
                writer.flush();
                writer.close();
                out.close();

                urlConnection.connect();

                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder responseOutput = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                }
                br.close();

                output = responseOutput.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return output;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);

            progressDialog.cancel();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (s.equals("ok")) {
                        showAlertDialog("We received your request and someone from our team will be in touch with you soon. Thank you!");

                        imageBitmap = null;
                        ibSelectImage.setImageResource(R.mipmap.camera);
                    } else
                        showAlertDialog(s);
                }
            });
        }
    }
}